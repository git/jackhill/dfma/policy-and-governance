# Durham FM Association Code of Conduct

Note: In this document, "DFMA" refers to the Durham FM
Association and "board members" refers to elected membership of the
Durham FM Association board.

## Our Pledge

In the interest of fostering an open and welcoming environment, we as
DFMA members pledge to making participation in our association and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
education, socio-economic status, nationality, personal appearance, race,
religion, or sexual identity and orientation.

## Our Standards

Examples of behavior that contributes to creating a positive environment include:


* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members


Examples of unacceptable behavior by participants include:


* The use of sexualized language or imagery and unwelcome sexual
  attention or advances
* Trolling, insulting/derogatory comments, and personal or political
  attacks
* Public or private harassment
* Publishing others’ private information without explicit permission
* Other conduct which could reasonably be considered inappropriate in
  a professional setting

Special considerations shall be applied to the DFMA repeaters because
of their nature as general communication media. In order to preserve
the availability and enjoyment of the repeaters for that purpose, the
following additional behaviors are unacceptable on the repeaters:

* Transmissions that are illegal or otherwise violate [FCC Part 97
  regulations][part-97]
* Failure to follow the DFMA Repeater Use Policy
* Personal antagonisms and Soap Boxing, which goes hand-in-hand with
  overly long conversations, is when people carry on a conversation on
  the repeater that is a thinly disguised broadcast. The subject is
  generally to "put down" an institution, group, or an individual for
  as wide as possible an audience
* Responding to or acknowledging jamming or interference
  
[part-97]: https://www.ecfr.gov/cgi-bin/text-idx?SID=2928fcde8b08bdf8e620efb797e75e65&mc=true&node=pt47.5.97&rgn=div5


## Our Responsibilities

Board members are responsible for publicizing and clarifying the
standards of acceptable behavior and are expected to take appropriate
and fair corrective action in response to any instances of
unacceptable behavior.

Board members have the right and responsibility to remove, edit, or
reject comments and other contributions that are not aligned to this
Code of Conduct, take action including disabling repeaters or other
communication technology, or to ban temporarily or permanently anyone
for other behaviors that they deem inappropriate, threatening,
offensive, or harmful.

## Scope

This Code of Conduct applies to use of and participation in DFMA
repeaters, online fora, public service, and DFMA events as well as in public spaces
when an individual is representing the DFMA or its community. Examples
of representing the DFMA or community include using an official email
address, posting online with official credentials, or acting as an
appointed representative. Representation of the DFMA may be further
defined and clarified by board members.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior
may be reported by contacting the board members at
dfma-board@km4mbg.org. All complaints will be reviewed and
investigated and will result in a response that is deemed necessary
and appropriate to the circumstances. Board members are obligated to
maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted
separately.

Board members should work to find an enforcement action that focuses
first on correction so that everyone can continue to enjoy
participating in the DFMA. However, when correction is not possible,
they shall take the necessary action to ensure the DFMA repeaters,
online fora, and events are free of the unacceptable behavior.

Board members who do not follow or enforce the Code of Conduct
in good faith may face temporary or permanent repercussions as
determined by other members of the DMFA leadership.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant,
version 1.4][ccovenant], which was created by Coraline Ada Ehmke in
2014 and is released under the [Creative Commons Atrribution 4.0
License][cc-by]. Additional ideas were taken from the [Arizona
Repeater Association][w7ara] and the [Alamo Area Radio
Organization][aa5ro].

[ccovenant]: https://www.contributor-covenant.org/version/1/4/code-of-conduct.html
[cc-by]: https://creativecommons.org/licenses/by/4.0/
[w7ara]: https://www.w7ara.org/z/Conduct.Aspx
[aa5ro]: https://www.aa5ro.org/the-aa5ro-repeaters/repeater-rules

## Additional Resources

* DFMA Repeater Use Policy
* OCRA Acceptable Use Policy
* [Mozilla's Diversity Equity and Inclusion work][moz-inclusion]
* [GNU Kind Communication Guidelines][gkcg]

[moz-inclusion]: https://github.com/mozilla/inclusion
[gkcg]: https://www.gnu.org/philosophy/kind-communication.html
